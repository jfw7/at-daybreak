import {
  PlaneBufferGeometry,
  Mesh,
  MeshPhongMaterial,
  AddOperation,
} from 'three';

const geometry = new PlaneBufferGeometry(window.innerWidth, window.innerHeight);
const material = new MeshPhongMaterial({
  combine: AddOperation,
  dithering: true,
  reflectivity: 0.5,
});
const mesh = new Mesh(geometry, material);

mesh.position.set(0, 0, 0);
mesh.receiveShadow = true;

export default mesh;
