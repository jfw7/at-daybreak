import {
  Color,
  Math as ThreeJSMath,
  Object3D,
  SpotLight,
  Vector3,
} from 'three';
import randomColor from 'randomcolor';
import { sample } from 'lodash';

export const COLOR_BLACK = 'COLOR_BLACK';
export const COLOR_COOL = 'COLOR_COOL';
export const COLOR_WARM = 'COLOR_WARM';
export const COLOR_RED = 'COLOR_RED';
export const COLOR_WHITE = 'COLOR_WHITE';

const getColor = (type) => {
  switch (type) {
    case COLOR_COOL:
      return randomColor({ luminosity: 'dark', hue: sample(['blue', 'purple']) });
    case COLOR_WARM:
      return randomColor({ luminosity: 'bright', hue: sample(['red', 'orange', 'yellow']) });
    case COLOR_WHITE:
      return randomColor({ luminosity: 'bright', hue: sample(['yellow', 'monochrome']) });
    case COLOR_RED:
      return randomColor({ hue: 'red' });
    case COLOR_BLACK:
      return '#000';
    default:
      return randomColor();
  }
};

export default (options = {}) => {
  const { colorType = null } = options;
  const spotlight = new SpotLight(getColor(colorType));
  spotlight.colorType = colorType;
  spotlight.position.set(
    ThreeJSMath.randFloatSpread(200),
    ThreeJSMath.randFloatSpread(200),
    25,
  );
  spotlight.target = new Object3D();
  spotlight.target.position.set(
    spotlight.position.x + ThreeJSMath.randFloatSpread(100),
    spotlight.position.y + ThreeJSMath.randFloatSpread(100),
    0,
  );

  spotlight.alpha = 1;
  spotlight.alphaDelta = ThreeJSMath.randFloat(0.00001, 0.00005);

  spotlight.setDest = () => {
    spotlight.dest = new Vector3(
      ThreeJSMath.randFloatSpread(200),
      ThreeJSMath.randFloatSpread(200),
      25,
    );
    spotlight.targetDest = new Vector3(
      spotlight.dest.x + ThreeJSMath.randFloatSpread(100),
      spotlight.dest.y + ThreeJSMath.randFloatSpread(100),
      0,
    );
    spotlight.colorDest = new Color(getColor(spotlight.colorType));
  };

  spotlight.angle = ThreeJSMath.randFloat(Math.PI / 4, Math.PI / 2);
  spotlight.penumbra = ThreeJSMath.randFloat(0.25, 0.75);
  // spotlight.decay = 2;
  return spotlight;
};
