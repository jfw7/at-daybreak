import { Math as ThreeJSMath } from 'three';

import camera from './camera';
import mesh from './mesh';
import renderer from './renderer';
import scene from './scene';
import getSpotlight, * as colors from './getSpotlight';

scene.add(mesh);

const spotlights = [];

for (let i = 0; i < 9; i += 1) {
  spotlights.push(getSpotlight({ colorType: colors.COLOR_BLACK }));
}

spotlights.forEach(spotlight => {
  scene.add(spotlight.target, spotlight);
  // scene.add(new SpotLightHelper(spotlight));
});

let section = 0;
document.ontouchstart = event => {
    section += 1;
    event.preventDefault();
}
document.onkeydown = (event) => {
  switch (event.key) {
    case 'Left':
    case 'ArrowLeft':
      section = Math.max(0, section - 1);
      console.log(section);
      event.preventDefault();
      break;
    case 'Right':
    case 'ArrowRight':
      section += 1;
      console.log(section);
      event.preventDefault();
      break;
    default:
      break;
  }
};

const render = () => {
  renderer.render(scene, camera);
};

const animate = () => {
  window.requestAnimationFrame(animate);
  spotlights.forEach((spotlight, i) => {
    switch (section) {
      case 0:
        spotlight.colorType = colors.COLOR_BLACK;
        break;
      case 1:
        if (spotlight.colorType === colors.COLOR_BLACK && (i === 0 || spotlight.alpha >= 0.03)) {
          spotlight.colorType = colors.COLOR_COOL;
          spotlight.setDest();
          spotlight.alpha = 0;
        }
        break;
      case 2:
        if (spotlight.colorType === colors.COLOR_BLACK) {
          spotlight.colorType = colors.COLOR_COOL;
        }
        if (spotlight.alphaDelta < 0.00006) {
          spotlight.alphaDelta = ThreeJSMath.randFloat(0.00005, 0.0005);
        }
        if (spotlight.alpha >= 0.05) {
          spotlight.alphaDelta = ThreeJSMath.clamp(
            spotlight.alphaDelta * 1.01,
            0,
            0.001,
          );
        }
        break;
      case 3:
        if (spotlight.colorType === colors.COLOR_COOL && i > 6 && spotlight.alpha >= 0.05) {
          spotlight.colorType = colors.COLOR_RED;
        }
        break;
      case 4:
        if (spotlight.colorType === colors.COLOR_COOL && i > 4 && spotlight.alpha >= 0.05) {
          spotlight.colorType = colors.COLOR_WARM;
        }
        break;
      case 5:
        spotlight.alphaDelta = 0;
        break;
      case 6:
        spotlight.alphaDelta = ThreeJSMath.randFloat(0.0005, 0.0001);
        if (spotlight.alpha >= 0.05) {
          spotlight.alphaDelta = ThreeJSMath.clamp(
            spotlight.alphaDelta * 1.01,
            0,
            0.001,
          );
        }
        break;
      case 7:
        spotlight.alphaDelta = 0;
        break;
      case 8:
        if (spotlight.colorType !== colors.COLOR_WHITE) {
          spotlight.colorType = colors.COLOR_WHITE;
          spotlight.alphaDelta = ThreeJSMath.randFloat(0.03, 0.08);
          spotlight.setDest();
        }
        spotlight.color.lerp(
          spotlight.colorDest,
          spotlight.alpha,
        );
        spotlight.alpha = ThreeJSMath.clamp(
          spotlight.alpha + spotlight.alphaDelta,
          spotlight.alpha,
          1,
        );
        return;
      case 9:
        if (spotlight.colorType !== colors.COLOR_BLACK) {
          spotlight.alpha = 0;
          spotlight.colorType = colors.COLOR_BLACK;
          spotlight.setDest();
        }
        spotlight.color.lerp(
          spotlight.colorDest,
          spotlight.alpha,
        );
        spotlight.alpha = ThreeJSMath.clamp(
          spotlight.alpha + spotlight.alphaDelta,
          spotlight.alpha,
          1,
        );
        return;
      case 10:
        if (spotlight.colorType === colors.COLOR_BLACK) {
          spotlight.alphaDelta = ThreeJSMath.randFloat(0.00003, 0.00008);
          spotlight.colorType = colors.COLOR_COOL;
          spotlight.setDest();
          spotlight.alpha = 0;
        }
        break;
      case 11:
        if (spotlight.colorType !== colors.COLOR_BLACK) {
          spotlight.alpha = 0;
          spotlight.alphaDelta = ThreeJSMath.randFloat(0.03, 0.08);
          spotlight.colorType = colors.COLOR_BLACK;
          spotlight.setDest();
        }
        spotlight.color.lerp(
          spotlight.colorDest,
          spotlight.alpha,
        );
        spotlight.alpha = ThreeJSMath.clamp(
          spotlight.alpha + spotlight.alphaDelta,
          spotlight.alpha,
          1,
        );
        return;
      default:
        break;
    }
    if (spotlight.alpha >= 0.05) {
      spotlight.setDest();
      spotlight.alpha = 0;
    }
    spotlight.position.lerp(
      spotlight.dest,
      spotlight.alpha,
    );
    spotlight.target.position.lerp(
      spotlight.targetDest,
      spotlight.alpha,
    );
    spotlight.color.lerp(
      spotlight.colorDest,
      spotlight.alpha,
    );
    spotlight.alpha += spotlight.alphaDelta;
  });
  render();
};

document.body.appendChild(renderer.domElement);
animate();
