// Use this file as a starting point for your project's .eslintrc.
// Copy this file, and add rule overrides as needed.
module.exports = {
  extends: 'airbnb/base',
  globals: {
    document: true,
    window: true,
  },
  rules: {
    'arrow-parens': 0,
    'no-param-reassign': 0,
  },
};
